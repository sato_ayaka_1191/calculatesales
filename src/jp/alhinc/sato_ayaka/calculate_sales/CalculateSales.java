package jp.alhinc.sato_ayaka.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		//各支店のリストを保持

		Map<String, String> names = new HashMap<>();
		Map<String, Long> amounts = new HashMap<>();
		try {

		    BufferedReader br = null;
			try {
                File branchLst = new File(args[0], "branch.lst");

                //ファイルが存在するかを確認
				if (!branchLst.exists()) {
					System.out.println("支店定義ファイルが存在しません");
					return;
				}

				br = new BufferedReader(new FileReader(branchLst));

		        String line;
                while ((line = br.readLine()) != null) {
                    String[] items = line.split(",");
                    if (items.length != 2 || !items[0].matches("\\d{3}") || items[1].isEmpty()) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}

                    names.put(items[0], items[1]);
                    amounts.put(items[0], 0L);
				}
			} finally {
				if(br != null) {
					br.close();
				}
			}

			//支店ごとに集計
			File directory = new File(args[0]);
			String[] filenames = directory.list();

		    List<Integer> rcdNumbers = new ArrayList<>();
			for (String filename : filenames) {
                if (!filename.matches("\\d{8}.rcd")) {
                    continue;
                }

                rcdNumbers.add(Integer.parseInt(filename.substring(0, 8)));

                try {
                    File rcdFile = new File(args[0], filename);
                    br = new BufferedReader(new FileReader(rcdFile));

                    String code = br.readLine();
                    long amount = Long.parseLong(br.readLine());

                    if(br.readLine() != null) {
                        System.out.println(filename + "のフォーマットが不正です");
                        return;
                    }

                    if(!names.containsKey(code)) {
                        System.out.println(filename + "の支店コードが不正です");
                        return;
                    }

                    amounts.put(code, amounts.get(code) + amount);
                    if(amounts.get(code).toString().length() > 10) {
                        System.out.println("合計金額が10桁を超えました");
                        return;
                    }
                } finally {
                    br.close();
				}
			}

            Collections.sort(rcdNumbers);
            int min = rcdNumbers.get(0);
            int max = rcdNumbers.get(rcdNumbers.size() - 1);
			if(min + rcdNumbers.size() - 1 != max) {
				System.out.println("ファイル名が連番になっていません");
				return;
			}

			//支店別集計ファイルを作成、ファイル内に出力
			File branchOut = new File(args[0], "branch.out");
            BufferedWriter bw = null;
            try {
                bw = new BufferedWriter(new FileWriter(branchOut));
                for(String key : names.keySet()) {
                    bw.write(key + "," + names.get(key) + "," + amounts.get(key));
                    bw.newLine();
                }
            } finally {
                if (bw != null) {
			        bw.close();
                }
            }

		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました");
		}
	}
}
